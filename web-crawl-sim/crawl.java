import java.io.*;
import java.net.*;
import java.util.*;
public class crawl {

  /* Do not change this base URL. All URLs for ths assignmetn are relative to this address */
   private static String baseURL = "https://www.csd.uwo.ca/faculty/solis/cs9668/test/";
   
   public static void main (String[] args) {

      URL theUrl, u;
      BufferedReader input;
      String s;
      String[] query;   // User specified query
      boolean inFile;   // Set tru if any of the query words is in the document
      HashMap<String, Boolean> url_map = new HashMap<String, Boolean>();
      HashMap<String, Boolean> query_map = new HashMap<String, Boolean>();
      boolean no_more_links = false;
      query = InOut.readQuery();
      int init = 0;
      int query_cnt = 0;
      try {              
      System.out.println(query.length); 
      theUrl = new URL(baseURL+"test.html");  // This is the complete URL of the test page
      url_map.put(theUrl.toString(), false);  // This is the first page

      for (int i = 0; i < query.length; i++) {
         query_map.put(query[i], false);
      }

      while (no_more_links == false) {
         
         input = new BufferedReader(new InputStreamReader(theUrl.openStream())); // Open URL for reading
         inFile = false;

            // search document for all the query words
            while ((s = input.readLine()) != null) {  // Read the document specified by theUrl
                  u = extractURL(s);
                  if (u != null) {
                    
                     if (url_map.containsKey(u.toString()) == false) {
                        url_map.put(u.toString(), false);
                     }
                  }
                  for (int i = 0; i < query.length; ++i)  // Check if any of the query words is in this line of the document
                     if ((s.toLowerCase()).indexOf(query[i].toLowerCase()) != -1) {
                        if (query_map.get(query[i]) == false) {
                           query_map.put(query[i], true);
                           ++query_cnt;
                        }
                        if (query_cnt == query.length) {
                           System.out.println("true");
                           inFile = true;
                        }
                  }
            }
            // check for and get ready for next document if url left to search
            for (int i = 0; i < query.length; i++) {
               query_map.put(query[i], false);
            }
            query_cnt = 0;
            int count = 0;
            System.out.println("URL beginning " + theUrl.toString());
            // check query map if we found some new urls and need to search
            if (url_map.get(theUrl.toString()) == false) {
               url_map.put(theUrl.toString(), true);
               Iterator hmIterator = url_map.entrySet().iterator();
               while (hmIterator.hasNext()) {
                  Map.Entry mapElement = (Map.Entry)hmIterator.next();
                  // System.out.println("brah " + mapElement.getKey() + " : " + mapElement.getValue());
                  if (url_map.get(mapElement.getKey()) == false) {
                     // System.out.println("map shit " + mapElement.getKey());
                     theUrl = new URL(mapElement.getKey().toString());
                     break;
                  }
               }
            }
            else {
               break;
            }
            if (url_map.size() == 1) {
               no_more_links = true;
               System.out.println("fuckkkkkkk");
            }

         if (inFile) InOut.printFileName(theUrl); // You MUST use this method to print an URL
            input.close();
            InOut.endListFiles(); // You MUST invoke this method before your program terminates
      }
      
      } catch (MalformedURLException mue) {
         System.out.println("Malformed URL");

      } catch (IOException ioe) {
         System.out.println("IOException "+ioe.getMessage());
      }     
   }
   
   /* If there is an URL embedded in the text passed as parameter, the URL will be extracted and
      returned; if there is no URL in the text, the value null is returned                       */   
   public static URL extractURL(String text) throws MalformedURLException {
   	String textUrl;
      	int index = text.lastIndexOf("a href=");
   	if (index > -1) {
   		textUrl = baseURL+text.substring(index+8,text.length()-2);   // Form the complete URL	
   		return new URL(textUrl);
   	}
   	else return null;
   }
} 


