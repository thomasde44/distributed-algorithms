import java.io.*;
import java.util.*;

public class ComputeRouting extends Algorithm {

    public Object run() {
        // Our sumTree algorithm returns the total of distances from the descendants to this node 
        String result = computeRoutingTable(getID());
        return result;
    }

    public String computeRoutingTable(String id) {

        Message m = null;
        int n = numNeighbours() - 1;
        Message mssg = null;

        boolean leaf = false;

        boolean cont = true;

        String data = id;

        int finish_cnt = 0;

        boolean leaf_finished = false;
        

        try {

            boolean done_array[] = new boolean[getChildren().size()];
            Arrays.fill(done_array, false);
      
	        RoutingTable table = new RoutingTable(id);
            Vector<String> v = getChildren();
			/* Your initialization code goes here */
            if (v.size() == 0) {
                    
                    // table.addEntry(id, id);
                    data = "leaf " + id + " " + id;
                    mssg = makeMessage(getParent(), data);
                    leaf_finished = true;
                    
                    // leaf = true;
            }
        
            while (waitForNextRound()) {  /* synchronous loop */
                
                // printMessage(Arrays.toString(done_array)); 
                // ==== send ====
                if (mssg != null) {
                    send(mssg);
                }

                if (leaf_finished == true) {
                    // table.printTables();
                    return "";
                }
                // printMessage(id + " " + done_array.length);
                for (int i = 0; i < done_array.length; i++) {
                    if (done_array[i] == true) {
                        finish_cnt++;
                    }
                    if (finish_cnt == done_array.length) {
                        table.printTables();
                        // printMessage(Arrays.toString(done_array) + " " + finish_cnt);
                        // printMessage(table.stringRepresentation("done", table));
                        return "";
                    }
                }
                finish_cnt = 0;
                mssg = null;
                cont = true;
                
                // ===== receive =====
                while (cont == true) {
                    m = receive();
                    if (m != null) {
                        
                        if (m.data().contains("leaf")) {
                            // table.addEntry(id, id);
                            // printMessage(m.data().split(" ")[1] + " " + m.data().split(" ")[2]);
                            table.addEntry(m.data().split(" ")[1], m.data().split(" ")[2]);

                            data = table.stringRepresentation("up", table);
                            data = data + " " + id;
                            String array[]= m.data().split(" ");
                            done_array[v.indexOf(array[array.length - 1])] = true;
                            // printMessage(mssg + "");
                            if (isRoot() == false) {
                                mssg = makeMessage(getParent(), data);
                            }
                            // finish_cnt++;
                        }

                        if (m.data().contains("up") && isRoot() == false) {

                            // printMessage(m.data.split(" ").size());
                            String array[]= m.data().split(" ");
                            // printMessage(array.length + "hi");
                            done_array[v.indexOf(array[array.length - 1])] = true;
                            table.addEntry(array[array.length - 1], array[array.length - 1]);
                            for (int i = 1; i < array.length; i++) {
                                // printMessage(array[i]);
                                if (i % 2 != 0 && i < array.length - 1) {
                                    table.addEntry(array[array.length - 1], array[i+1]);
                                }
                            }
                            data = table.stringRepresentation("up", table);
                            data = data + " " + id;
                            mssg = makeMessage(getParent(), data);
                            // finish_cnt++;
                            
                        }

                        if (m.data().contains("up") && isRoot() == true) {

                            
                            
                            // printMessage(m.data.split(" ").size());
                            String array[]= m.data().split(" ");
                            // printMessage(array.length + "hi" + (array.length - 1));
                            table.addEntry(array[array.length - 1], array[array.length - 1]);
                            done_array[v.indexOf(array[array.length - 1])] = true;
                            // table.addEntry(id, id);
                            for (int i = 1; i < array.length; i++) {
                                // printMessage(array[i]);
                                if (i % 2 != 0 && i < array.length - 1) {
                                    table.addEntry(array[array.length - 1], array[i+1]);
                                }
                            }
                            printMessage("fuccccckkk");
                            // finish_cnt++;
                        }

                        // printMessage(table.stringRepresentation("done", table));

                    }
                    else {
                        
                        cont = false;
                    }

                }

                // table.printTables();
                
			
            }
        } catch(SimulatorException e){
            System.out.println("ERROR: " + e.toString());
        }
    
        return "";
    }

}