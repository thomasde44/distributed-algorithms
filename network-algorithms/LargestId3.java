import java.io.*;
import java.util.*;
/* Algorithm for solving the leader election problem in a synchronous ring network. */

public class LargestId3 extends Algorithm {

    /* Do not modify this method */
    public Object run() {
	int largest = findLargest(getID());
        return largest;
    }
 
    public int findLargest(String id) {
        Vector<String> v = neighbours(); // Set of neighbours of this node.
        String left = v.elementAt(0);
        String right = v.elementAt(1);
        String max_known = id;
        String initial_data = "";
        Message mssg = makeMessage(left, initial_data);
        Message m = null;
        Message m2 = null;
        int flip_count = 0;
        // Your initialization code goes here
        int flip_cnt = 0;
        int clear = 0;
        String tmp = "";
        int clock = 0;
        int clock2 = 0;
        String data = id + "," + max_known;
        try {
            while (waitForNextRound()) { // Main loop. All processors wait here for the beginning of the next round.
                
                // ===Send Phase===
                if(mssg != null) { // If we have a message, send it!
                    send(mssg);
                }
                
                // ===Receive Phase===
                mssg = null;
                // printMessage(integerToString(round));


                m = receive();
                m2 = receive();
                // flip the neighbour 
                if (m2 != null) {

                    printMessage("bitch" + id);

                    if (left == m.source()){
                        printMessage("wtf");
                        mssg = makeMessage(m.source(), "flip");
                    }
                    if (left == m2.source()){
                        printMessage("wtf2");
                        mssg = makeMessage(m2.source(), "flip");
                    }
                    
                }
                if (m != null && m2 == null) {
                    // printMessage("fuck" + id);
                
                    if (contains(m.data(), "flip")) {
                            tmp = left;
                            left = right;
                            right = tmp;
                            tmp = "";
                            flip_cnt = flip_cnt + 1;
                            mssg = makeMessage(left, "");
                    }
                    // mssg = makeMessage(left, "");
                }
                if (m == null && m2 == null && clock == 0) {
                    mssg = null;
                }
                if (m == null && m2 == null && clock == 1 && clock2 < 9) {
                    mssg = makeMessage(left, "");
                }

                if (clock2 == 9) {
                    printMessage("hey");
                    mssg = makeMessage(left, data);
                }
                if (clock2 > 9 && m != null) {
                    if (compare(m.data().split(",")[0], id) == 0) {
                        max_known = m.data().split(",")[1];
                        printMessage(m.data());
                        return stringToInteger(max_known);
                    }

                    if (compare(m.data().split(",")[1], id) == 1) {
                        data = m.data().split(",")[0] + "," + m.data().split(",")[1];
                        mssg = makeMessage(left, data);
                    }
                    if (compare(m.data().split(",")[1], id) == -1) {
                        // data = m.data().split(",")[0] + "," + m.data().split(",")[1];
                        data = m.data().split(",")[0] + "," + id;
                        mssg = makeMessage(left, data);
                    }
                    if (compare(m.data().split(",")[1], id) == 0) {
                        // data = m.data().split(",")[0] + "," + m.data().split(",")[1];
                        mssg = makeMessage(left, m.data());
                    }
                }



                    



            printMessage(integerToString(clock2));
            clock = (clock + 1) % 2;
            clock2 = clock2 + 1;

            }
                
        } catch(SimulatorException e){
            System.out.println("ERROR: " + e.toString());
        }   
        // If we got here, something went wrong! (Exception, node failed, etc.)
		return 0;
    }
}