import java.util.Vector;
import java.util.Collections;
/* Algorithm for computing the diameter of a hub-and-spoke network. */

public class Diameter extends Algorithm {

    /* Do not modify this method */
    public Object run() {
        int d = findDiameter(getID());
        return d;
    }

    public int findDiameter(String id) {
        Vector<String> v = neighbours(); // Set of neighbours of this node.

        // mssg = <cnt, hub_id>
        Message mssg = null;
        int clock = 0;
        String data = "";
        int cnt = 0;
        String direction = "forward";
        int n = numNeighbours();
        Vector<String> results = new Vector<String>(n);
        // Vector<Message> messages = new Vector<Message>(n);
        // Vector<String> results = new Vector<String>(n);
        // for (int i = 1; i <= n; i++)
        //     messages.add(null);
        //     results.add("0");
        Message m = null;
        int round = 0;
        int tmp = 0;
        if (numNeighbours() > 2) {
            data = cnt + "," + id + "," + direction;
            mssg = makeMessage(v.elementAt(0), data);
        }



        try {
            while (waitForNextRound()) { // Main loop. All processors wait here for the beginning of the next round.
                // ===Send Phase===
                if(mssg != null) { // If we have a message, send it!
                        send(mssg);
                        if (contains(mssg.data(), "backward")){
                            return stringToInteger(mssg.data().split(",")[0]);
                        }
                }

                // ===Receive Phase===
                mssg = null;
                m = receive();
                if (m != null) {

                    if (numNeighbours() > 2) {
                        printMessage("hey");
                        if (compare(m.data().split(",")[1], id) == 0) {
                            if (round < (n-1)) {
                
                                printMessage(m.data().split(",")[0]);
                                tmp = Integer.parseInt(m.data().split(",")[0]); 
                                tmp = tmp + 1;
                                results.add("" + tmp);
                                round = round + 1;
                                data = "0" + "," + id + "," + "forward";
                                mssg = makeMessage(v.elementAt(round), data);
                                printMessage(results.elementAt(round -1));
                            }
                            else {
                                tmp = Integer.parseInt(m.data().split(",")[0]); 
                                tmp = tmp + 1;
                                results.add("" + tmp);
                                Collections.sort(results);
                                String one = results.elementAt(results.size() - 1);
                                String two = results.elementAt(results.size() - 2);
                                printMessage(integerToString(results.size()));
                                printMessage(one);
                                printMessage(two);
                                int ans = stringToInteger(one) + stringToInteger(two);
                                return ans;
                            }
                        }

                    }
                    if (numNeighbours() == 2) {
                        data = m.data().split(",")[0] + "," + m.data().split(",")[1] + "," + m.data().split(",")[2];
                        if (contains(m.data(), "backward")) {
                            tmp = Integer.parseInt(m.data().split(",")[0]); 
                            tmp = tmp + 1;
                            data = tmp + "," + m.data().split(",")[1] + "," + m.data().split(",")[2];
                        }
                        if (compare(m.source(), v.elementAt(0)) == 0) {
                            mssg = makeMessage(v.elementAt(1), data);
                        }
                        if (compare(m.source(), v.elementAt(1)) == 0) {
                            mssg = makeMessage(v.elementAt(0), data);
                        }
                     

                    }
                    if (numNeighbours() < 2) {

                            direction = "backward";
                            data = m.data().split(",")[0] + "," + m.data().split(",")[1] + "," + direction;
                            mssg = makeMessage(m.source(), data);
                        
                    }
                }

            }
        } catch(SimulatorException e){
            System.out.println("ERROR: " + e.toString());
        }
    
        // If we got here, something went wrong! (Exception, node failed, etc.)
        return 0;
    }
}
