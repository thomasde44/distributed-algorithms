# distributed algorithms

### To run ###

./compile your_file.java
./runsim network_config.txt


The simulator in the .jar file extends an included class called algorithm to test distributed network algorithms. The text file contains graph nodes and edges. The simulator has built in message class that runs in terms of rounds. Each round the nodes in the network can perform two operations first sending messages, second receving messages. 


#### Solving leader elction problem in a ring ####

largestId3.java

![Alt text](./leader_election.png?raw=true "Plotted eigen values")

#### Solving terminal leg length of hub and spoke network ####

Diameter.java


![Alt text](./hub_spoke.png?raw=true "Plotted eigen values")
