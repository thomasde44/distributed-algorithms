import java.util.Vector;            
public class Find extends Algorithm {
    private int m;                   	// Ring of identifiers has size 2^m
    private int SizeRing;              // SizeRing = 2^m
	String result = ""; 
    public Object run() {
        return find(getID());
    }

	// Each message sent by this algorithm has the form: flag, value, ID 
	// where:
	// - if flag = "GET" then the message is a request to get the document with the given key
	// - if flag = "LOOKUP" then the message is request to forward the message to the closest
	//   processor to the position of the key
	// - if flag = "FOUND" then the message contains the key and processor that stores it
	// - if flag = "NOT_FOUND" then the requested data is not in the system
	// - if flag = "END" the algorithm terminates
	
	/* Complete method find, which must implement the Chord search algorithm using finger tables 
	   and assumming that there are two processors in the system that received the same ring identifier. */ 
	/* ----------------------------- */
    public Object find(String id) {
	/* ------------------------------ */
       try {
       
             /* The following code will determine the keys to be stored in this processor, the keys that this processor
                needs to find (if any), and the addresses of the finger table                                           */
		Vector<Integer> searchKeys; 		// Keys that this processor needs to find in the P2P system. Only
										// for one processor this vector will not be empty
		Vector<Integer> localKeys;   		// Keys stored in this processor
		Message mssg = null, message;	
		localKeys = new Vector<Integer>();
		String[] fingerTable;                  // Addresses of the fingers are stored here
		searchKeys = keysToFind();             // Read keys and fingers from configuration file
		fingerTable = getKeysAndFingers(searchKeys,localKeys,id);  // Determine local keys, keys that need to be found, and fingers
		m = fingerTable.length-1;
		SizeRing = exp(2,m);
		Message r = null;
		String succ = successor();
		String[] data;
		int keyValue; 
		boolean keyProcessed = false;
	
		/* Your initialization code goes here */
		int hashID = hp(id);            	// Ring identifier for this processor
		int hashSucc = hp(succ);			// Ring identifier for the successor of this processor
		int search_cnt = 0;
		
		boolean flag = false;
		int finger_cnt = 1;
		Vector<String> n = neighbours();
		
		if (searchKeys.size() > 0) { 		// Get the next key to find and check if it is stored locally
			keyValue = searchKeys.elementAt(0);
			// searchKeys.remove(0);           // Do not search for the same key twice
			if (localKeys.contains(keyValue)) {
				result = result + keyValue + ":" + id + " "; // Store location of key in the result
				keyProcessed = true;
			}
			else // Key was not stored locally
				if (inSegment(hk(keyValue), hashID, hashSucc)) // Check if key must be stored in successor
					mssg = makeMessage (fingerTable[0], pack("GET",keyValue,id));
				else mssg = makeMessage (fingerTable[0], pack("LOOKUP",keyValue,id)); // Key is not in successor
		}

		while (waitForNextRound()) { // Synchronous loop
				

				/* Your code goes here */
				if (mssg != null) {
					send(mssg);
				}

			
				mssg = null;
				r = receive();
				while (r != null) {
					data = unpack(r.data());

					
						if (searchKeys.size() > 0) {
							keyValue = stringToInteger(data[1]);
							if (data[0].equals("GET")) {
								
							}
							if (data[0].equals("LOOKUP")) {
								if(localKeys.contains(data[1])) {
									if (inSegment(hk(keyValue), hashID, hashSucc)) {
										printMessage("message in segment");
										
									}
								}
							}
							if (data[0].equals("FOUND")) {
								printMessage("FOUNDDD");
								// keyValue = searchKeys.elementAt(0);
								result = result + data[1] + ":" + data[2] + " ";
								// search_cnt++;
								keyProcessed = true;
								
								
							}
						// check if another message has been receivec
							
						}

						if (data[0].equals("FOUND") && !data[2].equals(id)) {
							printMessage("FOUNDDD brah");
							// result = result + data[1] + ":" + data[2] + " ";
							for (int i = 0; i < n.size(); i++) {
									printMessage("neighbour " + n.elementAt(i));
									if (n.elementAt(i).equals(data[2])) {
										flag = true;
										break;
									}
								}
								if (flag) {
									mssg = makeMessage (data[2], pack("FOUND",data[1],data[2]));
								}
								else {
									mssg = makeMessage (n.elementAt(0), pack("FOUND",data[1],data[2]));
								}
						}

						if (data[0].equals("LOOKUP")) {
							keyValue = stringToInteger(data[1]);
							// printMessage("received");
							printMessage("hk value " + hk(keyValue));
							if (inSegment(hk(keyValue), hashID, hashSucc)) {
								printMessage("message in segment");
								
								
								for (int i = 0; i < n.size(); i++) {
									printMessage("neighbour " + n.elementAt(i));
									if (n.elementAt(i).equals(data[2])) {
										flag = true;
										break;
									}
								}
								if (flag) {
									mssg = makeMessage (data[2], pack("FOUND",data[1],data[2]));
								}
								else {
									mssg = makeMessage (n.elementAt(0), pack("FOUND",data[1],data[2]));
								}
							}
							else if (finger_cnt < m) {
								finger_cnt++;
								mssg = makeMessage (fingerTable[finger_cnt], pack("LOOKUP",keyValue,data[2]));
							}
						}
						r = receive();
						if (keyProcessed) { // Search for the next key
							searchKeys.remove(0);
							
							if (searchKeys.size() > 0) { // There are no more keys to find 
								keyValue = searchKeys.elementAt(0);
								printMessage("processed");
								if (inSegment(hk(keyValue), hashID, hashSucc)) // Check if key must be stored in successor
									mssg = makeMessage (fingerTable[1], pack("GET",keyValue,id));
								else mssg = makeMessage (fingerTable[1], pack("LOOKUP",keyValue,id));
							}
							if (searchKeys.size() == 0) {
								printMessage("DONE");
							}
						
								
						}
							
							
				}
			
		}
			  
 
        } catch(SimulatorException e){
            System.out.println("ERROR: " + e.toString());
        }
    
        /* At this point something likely went wrong. If you do not have a result you can return null */
        return null;
    }


	/* Determine the keys that need to be stored locally and the keys that the processor needs to find.
	   Negative keys returned by the simulator's method keysToFind() are to be stored locally in this 
           processor as positive numbers.                                                                    */
	/* ---------------------------------------------------------------------------------------------------- */
	private String[] getKeysAndFingers (Vector<Integer> searchKeys, Vector<Integer> localKeys, String id) throws SimulatorException {
	/* ---------------------------------------------------------------------------------------------------- */
		Vector<Integer>fingers = new Vector<Integer>();
		String[] fingerTable;
		String local = "";
		int m;
			
		if (searchKeys.size() > 0) {
			for (int i = 0; i < searchKeys.size();) {
				if (searchKeys.elementAt(i) < 0) {   	// Negative keys are the keys that must be stored locally
					localKeys.add(-searchKeys.elementAt(i));
					searchKeys.remove(i);
				}
				else if (searchKeys.elementAt(i) > 1000) {
					fingers.add(searchKeys.elementAt(i)-1000);
					searchKeys.remove(i);
				}
				else ++i;  // Key that needs to be searched for
			}
		}
			
		m = fingers.size();
		// Store the finger table in an array of Strings
		fingerTable = new String[m+1];
		for (int i = 0; i < m; ++i) fingerTable[i] = integerToString(fingers.elementAt(i));
		fingerTable[m] = id;
	
		for (int i = 0; i < localKeys.size(); ++i) local = local + localKeys.elementAt(i) + " ";
		showMessage(local); // Show in the simulator the keys stored in this processor
		return fingerTable;
	}

	    /* Determine whether hk(value) is in (hp(ID),hp(succ)] */
    /* ---------------------------------------------------------------- */
    private boolean inSegment(int hashValue, int hashID, int hashSucc) {
    /* ----------------------------------------------------------------- */
		if (hashID == hashSucc)
			if (hashValue == hashID) return true;
			else return false;
        else if (hashID < hashSucc) 
			if ((hashValue > hashID) && (hashValue <= hashSucc)) return true;
			else return false;
		else 
			if (((hashValue > hashID) && (hashValue < SizeRing)) || 
                ((0 <= hashValue) && (hashValue <= hashSucc)))  return true;
			else return false;
    }

    /* Hash function to map processor ids to ring identifiers. */
    /* ------------------------------- */
    private int hp(String ID) throws SimulatorException{
	/* ------------------------------- */
        return stringToInteger(ID) % SizeRing;
    }

    /* Hash function to map keys to ring identifiers */
    /* ------------------------------- */
    private int hk(int key) {
    /* ------------------------------- */
        return key % SizeRing;
    }

    /* Compute base^exponent ("base" to the power "exponent") */
    /* --------------------------------------- */
    private int exp(int base, int exponent) {
    /* --------------------------------------- */
        int i = 0;
        int result = 1;

		while (i < exponent) {
			result = result * base;
			++i;
		}
		return result;
    }
}
